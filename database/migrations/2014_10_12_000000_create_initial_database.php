<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('status');
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->integer('currentGameId')->unsigned()->nullable();
            $table->integer('lastGameId')->unsigned()->nullable();
            $table->bigInteger('currentGameScore')->nullable();
            $table->bigInteger('lastGameScore')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('currentGameId')->references('id')->on('games');
            $table->foreign('lastGameId')->references('id')->on('games');
        });

        Schema::create('searches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phrase');
            $table->integer('gameId')->unsigned();
            $table->integer('userId')->unsigned();
            $table->bigInteger('resultCount');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->foreign('gameId')->references('id')->on('games');
            $table->foreign('userId')->references('id')->on('users');

        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('games');
        Schema::drop('users');
        Schema::drop('searches');
        Schema::drop('password_resets');
    }
}
