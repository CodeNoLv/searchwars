<?php

return [
    'googleApiKey' => env('GAME__GOOGLE_API_KEY', 'forge'),
    'googleTestMode' => env('GAME__GOOGLE_TEST_MODE', 'forge'),
    'gameHandleInterval' => env('GAME__GAMES_INTERVAL', 'forge'),
];