@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content">

            @if ($hasAccess)
                <h3>Current game score: {{ $currentGameScore }}</h3>

                @if(Session::has('message'))
                    <div class="alert alert-info">
                        {{Session::get('message')}}
                    </div>
                @endif

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                     @endforeach
                </ul>

                {!! Form::open(array('route' => 'search-store', 'class' => 'form')) !!}

                <div class="form-group">
                    {!! Form::text('phrase', null,
                        array('required',
                              'id' => 'phrase',
                              'class'=>'form-control',
                              'placeholder'=>'Key phrase')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Search',
                      array('class'=>'btn btn-primary')) !!}
                </div>
                {!! Form::close() !!}
            @else
                <h2>You are not signed up for an active game.</h2>
            @endif
        </div>
    </div>
@endsection

<script>
    $("#phrase").focus();
</script>