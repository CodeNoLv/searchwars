@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content">
            <h2>Game ended!</h2>
            <h3>Your score: {{ $lastGameScore }}</h3>
            <a href="{{ URL::route('last-game-ranking') }}" class="btn btn-default">Last game ranking</a>
        </div>
    </div>
@endsection