@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session::has('message'))
            <div class="alert alert-info">
                {{Session::get('message')}}
            </div>
        @endif

        @if(Session::has('error'))
            <div class="alert alert-danger">
                {{Session::get('error')}}
            </div>
        @endif
        <div class="content">
            @if ($signedUp == true)
                <h3>You are in game lobby. Wait till the game starts.</h3>
                <a href="{{ URL::route('start-game') }}" class="btn btn-default">Play</a>
            @else
                <a href="{{ URL::route('get-in-game') }}" class="btn btn-default">Join next games lobby</a>
            @endif
        </div>
    </div>
@endsection