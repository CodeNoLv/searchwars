@extends('layouts.app')

@section('content')

    @if ($rankingIsOn == false)
        <h3>There are no last games</h3>
    @else
        <div class="panel panel-default">
            <div class="panel-heading">
                Ranking
            </div>

            @if ($ranking != false)
            <div class="panel-body">
                <table class="table table-striped task-table">
                    <thead>
                        <th>User</th>
                        <th>Score</th>
                    </thead>

                    <tbody>
                    @foreach ($ranking as $entry)
                        <tr>
                            <td class="table-text">
                                <div>{{ $entry['userName'] }}</div>
                            </td>
                            <td class="table-text">
                                <div>{{ $entry['score'] }}</div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    @endif
@endsection