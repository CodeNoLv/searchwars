# Description #
Search Wars is browser based game inspired by Search engines. Every X* minutes Users can join to the game lobby and wait for other players to join. When the game starts all players have search as popular phrases in Google search as they can think of. The more popular phrase is in google search the more points player receives. At the end of the game, everyone can see last games ranking with players and their scores.

X - cron job on php artisan game:handle interval


# Setup #

## .env file ##
To setup project, you need to create file in a root directory ".env" by ".env.example" example.

You must setup 3 project related env parameters:

**GAME__GOOGLE_API_KEY**=xxx - Google API key which will be used for custom search calls

**GAME__GOOGLE_TEST_MODE**=true - If you don't have or don't want to use Google API key for custom search, set this to true, and game will use mock objects instead of Google API calls.

**GAME__GAMES_INTERVAL**=5 - Server has to have cron job on command php artisan game:handle every x minutes. This parameter has to match with x.

## Cron jobs ##
Server has to have cron job every X minute on command php artisan game:handle. This command cyclically creates new game where users can join and starts previously created game. App needs to have defined GAME__GAMES_INTERVAL parameter in .env file equal to X.

Cron Job have to start at round hour. (Examples, 00:00, 01:00).

## Migrations ##
App uses MySql database. You have to set up MySql database connection and run migrations (php artisan migrate).