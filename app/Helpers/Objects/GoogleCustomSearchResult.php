<?php

namespace App\Helpers\Objects;

class GoogleCustomSearchResult
{
    /** @var String */
    public $phrase;

    /** @var Integer */
    public $resultCount;

    /**
     * @return String
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

    /**
     * @param String $phrase
     *
     * @return $this
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;

        return $this;
    }

    /**
     * @return int
     */
    public function getResultCount()
    {
        return $this->resultCount;
    }

    /**
     * @param int $resultCount
     *
     * @return $this
     */
    public function setResultCount($resultCount)
    {
        $this->resultCount = $resultCount;

        return $this;
    }
}