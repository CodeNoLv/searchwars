<?php

namespace App\Helpers;

use App\Game;
use App\Queries\UserModelQueries;
use App\UserModel;
use Auth;
use App\Search;
use App\Queries\GameQueries;
use Config;

class GameService
{
    /** @var GameQueries */
    private $gameQueries;

    /** @var UserModelQueries  */
    private $userModelQueries;

    public function __construct()
    {
        $this->gameQueries = new GameQueries();
        $this->userModelQueries = new UserModelQueries();
    }

    /**
     * @return Game
     */
    public function createNewGame()
    {
        $game = new Game();
        $game->setAttribute('status', Game::STATUS__PENDING);
        $game->save();

        return $game;
    }

    /**
     * @param String $phrase
     * @param Integer $resultCount
     *
     * @return Search
     */
    public function createNewSearch($phrase, $resultCount)
    {
        /** @var UserModel $userModel */
        $userModel = $this->userModelQueries->getUserModel(Auth::user()->id);
        $currentUserGame = $userModel->currentGame;

        // Create new search entry
        $search = new Search();
        $search->setAttribute('phrase', $phrase);
        $search->setAttribute('gameId', $currentUserGame->getAttribute('id'));
        $search->setAttribute('resultCount', $resultCount);
        $search->setAttribute('userId', Auth::user()->id);
        $search->save();

        // Update currentGameScore
        $newCurrentGameScore = $resultCount + Auth::user()->currentGameScore;
        $userModel->setAttribute('currentGameScore', $newCurrentGameScore);
        $userModel->save();

        return $search;
    }

    /**
     * @return bool
     */
    public function signUpUserForPendingGame()
    {
        /** @var Game $pendingGame */
        $pendingGame = $this->gameQueries->getCurrentlyPendingGame();

        if (!$pendingGame) {
            return false;
        }

        /** @var UserModel $userModel */
        $userModel = $this->userModelQueries->getUserModel(Auth::user()->id);
        $userModel->setAttribute('currentGameId', $pendingGame->getAttribute('id'));
        $userModel->save();

        return true;
    }

    /**
     * @return bool
     */
    public function isUserSignedUpForAciveGame()
    {
        /** @var Game $activeGame */
        $activeGame = $this->gameQueries->getCurrentlyActiveGame();

        if (!$activeGame) {
            return false;
        }

        if (!Auth::user()->currentGameId === $activeGame->id) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isUserSignedUpForPendingGame()
    {
        /** @var Game $pendingGame */
        $pendingGame = $this->gameQueries->getCurrentlyPendingGame();

        if (!$pendingGame) {
            return false;
        }

        if (Auth::user()->currentGameId !== $pendingGame->id) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isUsersCurrentGameActive()
    {
        return $this->gameQueries->isGameActive(Auth::user()->currentGameId);
    }

    /**
     * @param $currentGameId
     */
    public function resetGameUsers($currentGameId)
    {
        $this->userModelQueries->saveLastGameScoresForCurrentGameUsers($currentGameId);
        $this->userModelQueries->saveLastGameIdsForCurrentGameUsers($currentGameId);
        $this->userModelQueries->resetCurrentGameDataForCurrentGameUsers($currentGameId);
    }

    /**
     * @param Game $game
     *
     * @return array
     */
    public function getGameRanking(Game $game)
    {
        $ranking = array();

        if ($game->lastGameUsers->isEmpty()) {
            return $ranking;
        }

        /** @var  $lastActiveGameUsers */
        $lastActiveGameUsers = $game->lastGameUsers;
        $counter = 0;

        foreach ($lastActiveGameUsers as $user) {
            /** @var UserModel $user */

            $row = [
                'userName' => $user->name,
                'score' => $user->lastGameScore,
            ];

            $score[$counter] = $row['score']; // Needed for multisort
            $ranking[$counter] = $row;
            $counter++;
        }

        array_multisort($score, SORT_ASC, $ranking); // Sorts in correct score order

        return $ranking;
    }

    /**
     * @return \DateTime
     */
    public function getNextGameHandleTime()
    {
        $gameHandleInterval = Config::get('game.gameHandleInterval');
        date_default_timezone_set('Europe/Riga'); // TODO: Get user time zone from javascript

        // Get closest round time according to game handle interval
        $datetime = new \DateTime();
        $second = $datetime->format("s");
        $datetime->add(new \DateInterval("PT".(60-$second)."S"));
        $minute = $datetime->format("i");
        $minute = $minute % $gameHandleInterval;
        $diff = $gameHandleInterval - $minute;
        $datetime->add(new \DateInterval("PT".$diff."M"));

        return $datetime;
    }
}
