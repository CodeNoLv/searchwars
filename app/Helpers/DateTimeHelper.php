<?php

namespace App\Helpers;

class DateTimeHelper
{
    /**
     * @param \DateTime $dateTime
     *
     * @return string
     */
    public function getIntervalTillCurrentTime(\DateTime $dateTime)
    {
        $currentTime = new \DateTime();
        $gameEndTime = $currentTime->diff($dateTime);

        return $gameEndTime->format('%I:%S');
    }
}