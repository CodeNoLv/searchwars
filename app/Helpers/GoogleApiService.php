<?php

namespace App\Helpers;

use App\Helpers\Objects\GoogleCustomSearchResult;
use Config;

class GoogleApiService
{
    /**
     * @param String $phrase
     *
     * @return GoogleCustomSearchResult|null
     *
     * @throws \Exception
     */
    public function getCustomSearchResult($phrase)
    {
        $isGoogleApiTest = Config::get('game.googleTestMode');
        $resultArray = $this->requestCustomSearch($phrase);

        if (!$isGoogleApiTest) {
            if (array_key_exists('error', $resultArray)) {
                throw new \Exception('Google error: '.$resultArray['error']['errors'][0]['message']);
            }

            $resultObject = $this->parseResultArrayToResultObject($resultArray);
        } else {
            $resultObject = $this->createTestResultObject($phrase);
        }

        return $resultObject;
    }

    /**
     * @param String $phrase
     *
     * @return mixed
     */
    private function requestCustomSearch($phrase)
    {
        $requestLink = $this->buildRequestLink($phrase);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 2,
            CURLOPT_URL => $requestLink,
            CURLOPT_ENCODING => "UTF-8",
        ));

        $resp = curl_exec($curl);
        curl_close($curl);

        return json_decode($resp, true);
    }

    /**
     * @param array $resultArray
     *
     * @return GoogleCustomSearchResult
     */
    private function parseResultArrayToResultObject($resultArray)
    {
        $resultObject = new GoogleCustomSearchResult();
        $resultObject
            ->setPhrase($resultArray['queries']['request'][0]['searchTerms'])
            ->setResultCount($resultArray['queries']['request'][0]['totalResults']);

        return $resultObject;
    }

    /**
     * @param String $phrase
     *
     * @return GoogleCustomSearchResult
     */
    private function createTestResultObject($phrase)
    {
        $resultObject = new GoogleCustomSearchResult();
        $resultObject
            ->setPhrase($phrase)
            ->setResultCount(rand(0, 1000000));

        return $resultObject;
    }

    /**
     * @param String $phrase
     *
     * @return string
     */
    private function buildRequestLink($phrase)
    {
        $apiKey = Config::get('game.googleApiKey');

        $googleLink = "https://www.googleapis.com/customsearch/v1";

        $requestData = [
            'key' => $apiKey,
            'cx' => '017576662512468239146:omuauf_lfve',
            'q' => $phrase,
        ];

        return $googleLink."?".http_build_query($requestData);
    }
}