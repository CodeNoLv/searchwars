<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    public function currentGame()
    {
        return $this->belongsTo('App\Game', 'currentGameId', 'id');
    }

    public function lastGame()
    {
        return $this->belongsTo('App\Game', 'lastGameId', 'id');
    }

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
