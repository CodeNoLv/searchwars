<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    CONST STATUS__PENDING = 0;
    CONST STATUS__ACTIVE = 1;
    CONST STATUS__CLOSED = 2;

    /**
     * Get the search for the user
     */
    public function searches()
    {
        return $this->hasMany('App\Search', "gameId");
    }

    public function currentGameUsers()
    {
        return $this->hasMany('App\UserModel', 'currentGameId', 'id');
    }

    public function lastGameUsers()
    {
        return $this->hasMany('App\UserModel', 'lastGameId', 'id');
    }

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'games';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['userId'];
}
