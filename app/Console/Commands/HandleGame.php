<?php

namespace App\Console\Commands;

use App\Queries\GameQueries;
use Illuminate\Console\Command;
use App\Helpers\GameService;
use App\Game;

class HandleGame extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:handle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Handle current game';

    /** @var GameService */
    protected $gameService;

    /** @var GameQueries */
    protected $gameQueries;

    public function __construct(GameService $gameService, GameQueries $gameQueries)
    {
        parent::__construct();

        $this->gameService = $gameService;
        $this->gameQueries = $gameQueries;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var Game $activeGame */
        $activeGame = $this->gameQueries->getCurrentlyActiveGame();

        // Close currently active game
        if ($activeGame) {
            $activeGame->setAttribute('status', Game::STATUS__CLOSED);
            $activeGame->save();

            $this->gameService->resetGameUsers($activeGame->id);
            $this->gameService->createNewGame();

            $this->info('Currently active game with id '.$activeGame->id. ' closed');

            return true;
        }

        /** @var Game $pendingGame */
        $pendingGame = $this->gameQueries->getCurrentlyPendingGame();

        // Activate currently closed game
        if ($pendingGame) {
            $pendingGame->setAttribute('status', Game::STATUS__ACTIVE);
            $pendingGame->save();

            $this->info('Currently pending game with id '.$pendingGame->id. ' activated');

            return true;
        }

        $this->gameService->createNewGame(); // Create first game
        $this->info('Created first game');

        return true;
    }
}
