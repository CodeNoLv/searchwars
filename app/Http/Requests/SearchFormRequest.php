<?php

namespace App\Http\Requests;

class SearchFormRequest extends Request {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phrase' => 'required',
        ];
    }

}