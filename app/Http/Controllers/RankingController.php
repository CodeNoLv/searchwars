<?php

namespace App\Http\Controllers;

use App\Game;
use App\Helpers\GameService;
use App\Http\Requests\SearchFormRequest;
use App\Queries\GameQueries;
use App\Search;
use Auth;

class RankingController extends Controller
{
    /** @var GameService */
    private $gameService;

    /** @var GameQueries */
    private $gameQueries;

    public function __construct(GameService $gameService, GameQueries $gameQueries)
    {
        $this->gameService = $gameService;
        $this->gameQueries = $gameQueries;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lastGameRanking()
    {
        $rankingIsOn = false;
        $ranking = null;
        $lastActiveGame = $this->gameQueries->getLastActiveGame();

        if ($lastActiveGame !== false) {
            $ranking = $this->gameService->getGameRanking($lastActiveGame);
            $rankingIsOn = true;
        }

        return view('ranking', [
            'ranking' => $ranking,
            'rankingIsOn' => $rankingIsOn,
        ]);
    }
}