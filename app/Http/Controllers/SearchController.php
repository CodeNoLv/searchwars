<?php

namespace App\Http\Controllers;

use App\Helpers\DateTimeHelper;
use App\Helpers\GameService;
use App\Helpers\GoogleApiService;
use App\Helpers\Objects\GoogleCustomSearchResult;
use App\Http\Requests\SearchFormRequest;
use Auth;

class SearchController extends Controller
{
    /** @var GameService  */
    private $gameService;

    /** @var GoogleApiService  */
    private $googleApiService;

    /** @var DateTimeHelper */
    private $dateTimeHelper;

    public function __construct(
        GameService $gameService,
        GoogleApiService $googleApiService,
        DateTimeHelper $dateTimeHelper
    ) {
        $this->gameService = $gameService;
        $this->googleApiService = $googleApiService;
        $this->dateTimeHelper = $dateTimeHelper;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $hasAccess = false;
        $currentGameScore = null;

        if ($this->gameService->isUserSignedUpForAciveGame()){
            $hasAccess = true;
            $currentGameScore = Auth::user()->currentGameScore;
        }

        return view('search', [
            'hasAccess' => $hasAccess,
            'currentGameScore' => $currentGameScore,
        ]);
    }

    /**
     * @param SearchFormRequest $request
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function searchStore(SearchFormRequest $request)
    {
        // Has game ended
        if (!$this->gameService->isUsersCurrentGameActive()){
            return \Redirect::route('game-over')
                ->with('lastGameScore', Auth::user()->lastGameScore);
        }

        $phrase = $request->input('phrase');

        /** @var GoogleCustomSearchResult $googleResultObject */
        $googleResultObject = $this->googleApiService->getCustomSearchResult($phrase);
        $resultCount = $googleResultObject->getResultCount();

        $nextGameHandleTime = $this->gameService->getNextGameHandleTime();
        $gameEndTime = $this->dateTimeHelper->getIntervalTillCurrentTime($nextGameHandleTime);

        $this->gameService->createNewSearch($phrase, $resultCount);

        return \Redirect::route('search')
            ->with('message', 'You serached "'.$phrase.'" and google found '.$resultCount.' results. Game will end after '.$gameEndTime.' minutes');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function gameOver()
    {
        return view('game-over', ['lastGameScore' => Auth::user()->lastGameScore]);
    }
}