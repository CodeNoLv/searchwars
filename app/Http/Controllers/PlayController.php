<?php

namespace App\Http\Controllers;

use App\Helpers\GameService;

class PlayController extends Controller
{
    /** @var GameService  */
    private $gameService;

    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $signedUp = $this->gameService->isUserSignedUpForPendingGame();

        return view('play', [
            'signedUp' => $signedUp,
        ]);
    }

    /**
     * @return mixed
     */
    public function getInGame()
    {
        $signedUp = $this->gameService->signUpUserForPendingGame();
        $nextGameHandleTime = $this->gameService->getNextGameHandleTime();
        $time = $nextGameHandleTime->format('G:i');

        if ($signedUp) {
            return \Redirect::route('play')->with('message', 'Next game will start: '.$time.'. Press "Play" when game has started.');
        } else {
            return \Redirect::route('play')->with('error', 'Current game will end: '.$time.'. Press "Join next game lobby" when current game has ended.');
        }
    }

    /**
     * @return mixed
     */
    public function startGame()
    {
        $usersGameHasStarted = $this->gameService->isUserSignedUpForAciveGame();
        $nextGameHandleTime = $this->gameService->getNextGameHandleTime();
        $time = $nextGameHandleTime->format('H:i');

        if ($usersGameHasStarted){
            return \Redirect::route('search');
        } else {
            return \Redirect::route('play')->with('error', 'Waiting for other players! Next game will start: '.$time.'. Press "Play" when game has started.');
        }
    }
}