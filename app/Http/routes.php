<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('last-game-ranking', ['as' => 'last-game-ranking', 'uses' => 'RankingController@lastGameRanking']);
Route::get('get-in-game', ['as' => 'get-in-game', 'uses' => 'PlayController@getInGame']);
Route::get('start-game', ['as' => 'start-game', 'uses' => 'PlayController@startGame']);
Route::get('play', ['as' => 'play', 'uses' => 'PlayController@index']);
Route::get('game-over', ['as' => 'game-over', 'uses' => 'SearchController@gameOver']);
Route::get('search', ['as' => 'search', 'uses' => 'SearchController@search']);
Route::post('search-store', ['as' => 'search-store', 'uses' => 'SearchController@searchStore']);

// Authentication
Route::controllers([
    'password' => 'Auth\PasswordController',
]);
Route::auth();

Route::get('/home', 'HomeController@index');
