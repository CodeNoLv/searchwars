<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    /**
     * Get the game that owns the serach
     */
    public function game()
    {
        return $this->belongsTo('App\Game', 'gameId');
    }

    /**
     * Get the game that owns the serach
     */
    public function user()
    {
        return $this->belongsTo('App\UserModel', 'userId');
    }

    /**
    * @var bool
    */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'searches';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
