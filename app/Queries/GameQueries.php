<?php

namespace App\Queries;

use App\Game;

class GameQueries
{
    public function getCurrentlyPendingGame()
    {
        $game = Game::where('status', '=', Game::STATUS__PENDING)->first();

        if (!$game) {
            return false;
        }

        return $game;
    }

    /**
     * @return Game|bool
     */
    public function getCurrentlyActiveGame()
    {
        /** @var Game $game */
        $game = Game::where('status', '=', Game::STATUS__ACTIVE)->first();

        if (!$game) {
            return false;
        }

        return $game;
    }

    /**
     * @param Integer $gameId
     *
     * @return bool
     */
    public function isGameActive($gameId)
    {
        $matchThese = [
            'id' => $gameId,
            'status' => Game::STATUS__ACTIVE,
        ];

        $game = Game::where($matchThese)->first();

        if (!$game) {
            return false;
        }

        return $game;
    }

    /**
     * @return Game|bool
     */
    public function getLastActiveGame()
    {
        /** @var Game $lastActiveGame */
        $lastActiveGame = Game::where('status', Game::STATUS__CLOSED)
            ->orderBy('id', 'desc')
            ->first();

        if (!$lastActiveGame) {
            return false;
        }

        return $lastActiveGame;
    }
}