<?php

namespace App\Queries;

use App\UserModel;
use Mockery\CountValidator\Exception;
use DB;

class UserModelQueries
{
    public function getUserModel($userId)
    {
        $userModel = UserModel::find($userId);

        if (!$userModel) {
            return false;
        }

        return $userModel;
    }

    /**
     * @param Integer $currentGameId
     */
    public function resetCurrentGameDataForCurrentGameUsers($currentGameId)
    {
        $userTable = (new UserModel())->getTable();
        DB::table($userTable)
            ->where('currentGameId', '=', $currentGameId)
            ->update(
                [
                    'currentGameId' => null,
                    'currentGameScore' => 0,
                ]
            );
    }


    /**
     * @param Integer $currentGameId
     *
     * @return bool
     */
    public function saveLastGameScoresForCurrentGameUsers($currentGameId)
    {
        $userModels = UserModel::where('currentGameId', $currentGameId)->get();

        if (empty($userModels)) {
            return false;
        }

        foreach ($userModels as $user) {
            /** @var UserModel $user */
            $user->setAttribute('lastGameScore', $user->getAttribute('currentGameScore'));
            $user->save();
        }

        return true;
    }

    /**
     * @param Integer $currentGameId
     *
     * @return bool
     */
    public function saveLastGameIdsForCurrentGameUsers($currentGameId)
    {
        $userModels = UserModel::where('currentGameId', $currentGameId)->get();

        if (empty($userModels)) {
            return false;
        }

        foreach ($userModels as $user) {
            /** @var UserModel $user */
            $user->setAttribute('lastGameId', $currentGameId);
            $user->save();
        }

        return true;
    }
}